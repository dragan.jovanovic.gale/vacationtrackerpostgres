package com.ftninformatika.jwd.vacationTracker.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ftninformatika.jwd.vacationTracker.model.TotalVacationDaysPerYear;

public interface TotalVacationDaysPerYearRepository extends JpaRepository<TotalVacationDaysPerYear, Long> {

	Page<TotalVacationDaysPerYear> findByKorisnikId(Long korisnikId, Pageable p);

	TotalVacationDaysPerYear findOneById(Long id);

	TotalVacationDaysPerYear findByKorisnikKorisnickoImeAndYear(String korisnickoIme, int year);

}
