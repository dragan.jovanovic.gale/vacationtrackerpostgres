package com.ftninformatika.jwd.vacationTracker.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.ftninformatika.jwd.vacationTracker.enumer.KorisnickaUloga;

@Entity
public class Korisnik {
	@Id
	@SequenceGenerator(name = "korisnik_seq", sequenceName = "korisnik_seq", allocationSize = 1, initialValue = 5)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "korisnik_seq")
	private Long id;

	@Column(unique = true, nullable = false)
	private String korisnickoIme;

	@Column(unique = true, nullable = false)
	private String eMail;

	@Column(nullable = false)
	private String lozinka;

	@OneToMany(mappedBy = "korisnik", cascade = CascadeType.ALL)
	private List<TotalVacationDaysPerYear> totalVacationDayPerYear = new ArrayList<>();

	@OneToMany(mappedBy = "korisnik", cascade = CascadeType.ALL)
	private List<UsedVacationDays> usedVacationDays = new ArrayList<>();

	@Enumerated(EnumType.STRING)
	private KorisnickaUloga uloga;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public KorisnickaUloga getUloga() {
		return uloga;
	}

	public void setUloga(KorisnickaUloga uloga) {
		this.uloga = uloga;
	}

	public List<TotalVacationDaysPerYear> getTotalVacationDayPerYear() {
		return totalVacationDayPerYear;
	}

	public void setTotalVacationDayPerYear(List<TotalVacationDaysPerYear> totalVacationDayPerYear) {
		this.totalVacationDayPerYear = totalVacationDayPerYear;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Korisnik other = (Korisnik) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Korisnik [id=" + id + ", KorisnickoIme=" + korisnickoIme + " email" + eMail + "]";
	}

}
