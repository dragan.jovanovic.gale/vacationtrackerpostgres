package com.ftninformatika.jwd.vacationTracker.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ftninformatika.jwd.vacationTracker.dto.UsedVacationDaysDto;
import com.ftninformatika.jwd.vacationTracker.impl.JpaUsedVacationDaysImpl;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.service.CsvFileService;
import com.ftninformatika.jwd.vacationTracker.service.UsedVacationDaysService;
import com.ftninformatika.jwd.vacationTracker.support.ResponseMessage;
import com.ftninformatika.jwd.vacationTracker.support.UsedVacationDaysDtoToUsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.support.UsedVacationDaysToDto;

@Controller
@RequestMapping("/api/usedVacation")
public class UsedVacationDaysController {

	@Autowired
	private CsvFileService fileService;

	@Autowired
	private UsedVacationDaysDtoToUsedVacationDays toEntity;

	@Autowired
	private UsedVacationDaysService service;

	@Autowired
	private UsedVacationDaysToDto toDto;

	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/upload")
	public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
		String message = "";

		if (JpaUsedVacationDaysImpl.hasCSVFormat(file)) {
			try {
				fileService.saveUsedVacationDays(file);

				message = "Uploaded the file successfully: " + file.getOriginalFilename();
				return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
			} catch (Exception e) {
				message = "Could not upload the file: " + file.getOriginalFilename() + "!";
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
			}
		}

		message = "Please upload a csv file!";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
	}

	@PreAuthorize("hasRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UsedVacationDaysDto> create(@Valid @RequestBody UsedVacationDaysDto usedVacationDaysDto) {
		UsedVacationDays usedVacationDay = toEntity.convert(usedVacationDaysDto);

		UsedVacationDays savedUsedVacationDay = service.save(usedVacationDay);

		return new ResponseEntity<>(toDto.convert(savedUsedVacationDay), HttpStatus.CREATED);
	}

	@PreAuthorize("hasRole('KORISNIK')")
	@GetMapping
	public ResponseEntity<List<UsedVacationDaysDto>> getAll(@RequestParam() String startDate,
			@RequestParam() String endDate, @RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {

		LocalDate startDateDate = getLocalDate(startDate);
		LocalDate endDateDate = getLocalDate(endDate);

		String username = SecurityContextHolder.getContext().getAuthentication().getName();

		Page<UsedVacationDays> page = service.findByDatesAndKorisnikKorisnickoIme(username, startDateDate, endDateDate,
				pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);

	}

	private LocalDate getLocalDate(String dateTime) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return LocalDate.parse(dateTime, formatter);
	}

}
