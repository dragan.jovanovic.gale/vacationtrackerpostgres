package com.ftninformatika.jwd.vacationTracker.enumer;

public enum KorisnickaUloga {
    ADMIN,
    KORISNIK
}
