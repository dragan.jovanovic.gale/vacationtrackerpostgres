package com.ftninformatika.jwd.vacationTracker.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class TotalVacationDaysPerYear {

	@Id
	@SequenceGenerator(name = "total_vacation_days_per_year_seq", sequenceName = "total_vacation_days_per_year_seq", allocationSize = 1, initialValue = 6)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "total_vacation_days_per_year_seq")
	private Long id;

	@Column
	private int year;

	@Column
	private int numberOfDays;

	@ManyToOne
	private Korisnik korisnik;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getNumberOfDays() {
		return numberOfDays;
	}

	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public String toString() {
		return "TotalVacationDaysPerYear [id=" + id + ", year=" + year + ", numberOfDays=" + numberOfDays
				+ ", korisnik=" + korisnik + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, korisnik, numberOfDays, year);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TotalVacationDaysPerYear other = (TotalVacationDaysPerYear) obj;
		return Objects.equals(id, other.id) && Objects.equals(korisnik, other.korisnik)
				&& numberOfDays == other.numberOfDays && year == other.year;
	}
	
	
	

}
