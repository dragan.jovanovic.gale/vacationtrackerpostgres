package com.ftninformatika.jwd.vacationTracker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftninformatika.jwd.vacationTracker.dto.TotalAvailableAndUsedDaysDto;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.service.TotalVacationDaysPerYearService;
import com.ftninformatika.jwd.vacationTracker.service.UsedVacationDaysService;

@Controller
@RequestMapping("/api/AllDays")
public class TotalAvailableUsedDaysController {

	@Autowired
	private TotalVacationDaysPerYearService totalVacationDaysPerYearService;

	@Autowired
	private UsedVacationDaysService usedVacationDaysService;

	@PreAuthorize("hasRole('KORISNIK')")
	@GetMapping
	public ResponseEntity<TotalAvailableAndUsedDaysDto> getAll(@RequestParam() int year,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {

		String username = SecurityContextHolder.getContext().getAuthentication().getName();

		int totalDaysInYear = totalVacationDaysPerYearService.findByKorisnikKorisnickoImeAndYear(username, year)
				.getNumberOfDays();

		List<UsedVacationDays> usedDaysList = usedVacationDaysService.findByKorisnikKorisnickoImeAndYear(username,
				year);

		int usedDaysInYear = 0;

		for (UsedVacationDays usedDays : usedDaysList) {
			usedDaysInYear = usedDaysInYear + usedDays.getNumberOfDaysUsed();
		}

		int numberOfRemainingFreeDays = totalDaysInYear - usedDaysInYear;

		TotalAvailableAndUsedDaysDto toDto = new TotalAvailableAndUsedDaysDto();
		toDto.setTotalDays(totalDaysInYear);

		toDto.setAvailableDays(numberOfRemainingFreeDays);

		toDto.setUsedDays(usedDaysInYear);

		return new ResponseEntity<>(toDto, HttpStatus.OK);

	}
}
