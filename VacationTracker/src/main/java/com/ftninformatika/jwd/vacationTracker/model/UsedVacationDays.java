package com.ftninformatika.jwd.vacationTracker.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class UsedVacationDays {

	@Id
	@SequenceGenerator(name = "used_vacation_days_seq", sequenceName = "used_vacation_days_seq", allocationSize = 1, initialValue = 7)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "used_vacation_days_seq")
	private Long id;

	@Column
	private LocalDate startOfVacation;

	@Column
	private LocalDate endOfVacation;

	@ManyToOne
	private Korisnik korisnik;

	@Column
	private int year;

	@Column
	private int numberOfDaysUsed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getStartOfVacation() {
		return startOfVacation;
	}

	public void setStartOfVacation(LocalDate startOfVacation) {
		this.startOfVacation = startOfVacation;
	}

	public LocalDate getEndOfVacation() {
		return endOfVacation;
	}

	public void setEndOfVacation(LocalDate endOfVacation) {
		this.endOfVacation = endOfVacation;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getNumberOfDaysUsed() {
		return numberOfDaysUsed;
	}

	public void setNumberOfDaysUsed(int numberOfDaysUsed) {
		this.numberOfDaysUsed = numberOfDaysUsed;
	}

}
