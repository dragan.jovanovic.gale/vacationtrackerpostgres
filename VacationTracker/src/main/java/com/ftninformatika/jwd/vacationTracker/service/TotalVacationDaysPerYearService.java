package com.ftninformatika.jwd.vacationTracker.service;

import java.io.InputStream;
import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.vacationTracker.model.TotalVacationDaysPerYear;

public interface TotalVacationDaysPerYearService {

	TotalVacationDaysPerYear delete(Long id);

	Page<TotalVacationDaysPerYear> find(Long korisnikId, Integer pageNo);

	TotalVacationDaysPerYear findOneById(Long id);

	TotalVacationDaysPerYear findByKorisnikKorisnickoImeAndYear(String userName, int year);

	TotalVacationDaysPerYear save(TotalVacationDaysPerYear totalVacationDaysPerYear);

	TotalVacationDaysPerYear update(TotalVacationDaysPerYear totalVacationDaysPerYear);

	List<TotalVacationDaysPerYear> loadVacationDays(InputStream is);

}
