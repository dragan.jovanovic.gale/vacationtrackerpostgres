package com.ftninformatika.jwd.vacationTracker.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ftninformatika.jwd.vacationTracker.model.Korisnik;
import com.ftninformatika.jwd.vacationTracker.model.TotalVacationDaysPerYear;
import com.ftninformatika.jwd.vacationTracker.model.UsedVacationDays;
import com.ftninformatika.jwd.vacationTracker.repository.KorisnikRepository;
import com.ftninformatika.jwd.vacationTracker.repository.TotalVacationDaysPerYearRepository;
import com.ftninformatika.jwd.vacationTracker.repository.UsedDaysRepository;
import com.ftninformatika.jwd.vacationTracker.service.CsvFileService;
import com.ftninformatika.jwd.vacationTracker.service.KorisnikService;
import com.ftninformatika.jwd.vacationTracker.service.TotalVacationDaysPerYearService;
import com.ftninformatika.jwd.vacationTracker.service.UsedVacationDaysService;

@Service
public class JpaCsvFileServiceImpl implements CsvFileService {

	@Autowired
	private TotalVacationDaysPerYearService vacationService;

	@Autowired
	private TotalVacationDaysPerYearRepository vacationRepository;

	@Autowired
	private KorisnikService korisnikService;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private UsedVacationDaysService usedVacationDaysService;

	@Autowired
	private UsedDaysRepository usedVacationDaysRepository;

	@Override
	public void saveVacationDays(MultipartFile file) {
		// TODO Auto-generated method stub
		try {
			List<TotalVacationDaysPerYear> vacationDays = vacationService.loadVacationDays(file.getInputStream());
			vacationRepository.saveAll(vacationDays);
		} catch (IOException e) {
			throw new RuntimeException("fail to store csv data: " + e.getMessage());
		}
	}

	@Override
	public void saveUsers(MultipartFile file) {
		// TODO Auto-generated method stub
		try {
			List<Korisnik> users = korisnikService.loadUsers(file.getInputStream());
			korisnikRepository.saveAll(users);
		} catch (IOException e) {
			throw new RuntimeException("fail to store csv data: " + e.getMessage());
		}
	}

	@Override
	public void saveUsedVacationDays(MultipartFile file) {
		// TODO Auto-generated method stub
		try {
			List<UsedVacationDays> usedVacationDays = usedVacationDaysService.loadUsedDays(file.getInputStream());
			usedVacationDaysRepository.saveAll(usedVacationDays);
		} catch (IOException e) {
			throw new RuntimeException("fail to store csv data: " + e.getMessage());
		}
	}

}
